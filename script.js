let password = document.getElementById('password');
let passwordConfirm = document.getElementById('passwordConfirm');
let showPass = document.getElementById('showSymbol');
let hidePass = document.getElementById('hideSymbol');
let btn = document.querySelector('.btn');
let error = document.getElementById("error");

showPass.addEventListener('click', function() {
    if(password.type === "password"){
        password.type = "text";
        showPass.classList.add('fa-eye-slash');
        showPass.classList.remove('fa-eye');
    }else {
        password.type = "password";
        showPass.classList.remove('fa-eye-slash');
        showPass.classList.add('fa-eye');
    }
});
hidePass.addEventListener('click', function() {
    if(passwordConfirm.type === "password"){
        passwordConfirm.type = "text";
        hidePass.classList.add('fa-eye-slash');
        hidePass.classList.remove('fa-eye');
    }else {
        passwordConfirm.type = "password";
        hidePass.classList.remove('fa-eye-slash');
        hidePass.classList.add('fa-eye');
    }
});

btn.addEventListener('click', function (event) {
    if(password.value !== passwordConfirm.value) {
        error.innerHTML = 'Нужно ввести одинаковые значения';
        error.classList.add('text-warning');
        error.classList.remove('hide');
    } else if(
        password.value === passwordConfirm.value 
        && password.value !== "" 
        && passwordConfirm.value !== "") {
        error.remove();
        alert("You are welcome");
    }
    event.preventDefault();
});
